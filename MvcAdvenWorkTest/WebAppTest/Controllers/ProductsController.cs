﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebAppTest.Models;

namespace WebAppTest.Controllers
{
    public class ProductsController : Controller
    {
        
        public ActionResult Index()
        {
            
            var items = GetProducts();
            return View(items);
        }

        private List<Product> GetProducts()
        {
            List<Product> list = new List<Product>();



            string connStr = "Server=tcp:hillstartest.database.windows.net,1433;Initial Catalog=AdventureWorksLT;" +
                                   "Persist Security Info=False;User ID=elvin;Password={password};" +
                                "MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

            SqlConnection conn = new SqlConnection(connStr);
           

            //SqlCommand cmd = new SqlCommand("SELECT sod.SalesOrderID, soh.OrderDate, soh.ShipDate, sod.ProductID, sod.LineTotal FROM SalesLT.SalesOrderHeader AS soh JOIN SalesLT.SalesOrderDetail AS sod ON sod.SalesOrderID = soh.SalesOrderID JOIN SalesLT.Product p ON p.ProductID = sod.ProductID JOIN SalesLT.ProductCategory AS pc ON pc.ProductCategoryID = p.ProductCategoryID WHERE pc.name = 'Mountain Bikes'", conn);

            SqlCommand cmd = new SqlCommand("SELECT * from SalesLT.product", conn);

            cmd.CommandType = CommandType.Text;
            
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            
            while (dr.Read())
            {
                Product product = new Product();

                product.ProductID = Convert.ToInt32(dr["ProductID"]);
                product.Name = Convert.ToString(dr["Name"]);
                product.ProductNumber = Convert.ToString(dr["ProductNumber"]);
                product.StandardCost = Convert.ToDecimal(dr["StandardCost"]);
                product.ListPrice = Convert.ToDecimal(dr["ListPrice"]);
                product.Color = Convert.ToString(dr["Color"]);
                product.SellStartDate = Convert.ToDateTime(dr["SellStartDate"]);
                product.Size = Convert.ToString(dr["Size"]);
              

                list.Add(product);
            }   
          

            return list;

         }
        }

            
}
