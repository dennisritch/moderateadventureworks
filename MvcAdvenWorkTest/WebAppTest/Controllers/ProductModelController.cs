﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebAppTest.Controllers
{
    public class ProductModelController : Controller
    {
        // GET: ProductModel
        public ActionResult Index()
        {
            return View();
        }

        // GET: ProductModel/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: ProductModel/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ProductModel/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: ProductModel/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: ProductModel/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: ProductModel/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: ProductModel/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
